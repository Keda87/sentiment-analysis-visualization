# -*- coding: utf-8 -*-
import sys

import tweepy

from preproses import Preproses

prep = Preproses()

consumer_key = 'aAqQJ655lsfkSniNbl3dN2cKu'
consumer_secret = 'wbD8AIIHNXnzWQsZKJofYOQc4My0LQze6tP8CcpAodetvhT9yi'

auth = tweepy.AppAuthHandler(consumer_key, consumer_secret)
api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
if not api:
    sys.exit('Autentikasi gagal, mohon cek "Consumer Key" & "Consumer Secret" Twitter anda')

qry = ['fadli zon']

max_tweets = 10  # up to you
tweets_per_qry = 10  # No more than 100
file_name = 'Hasil_Tweets.csv'  # Nama File hasil Crawling


def populate_tweets(keyword):
    query = keyword
    sinceId, max_id, tweetCount = None, -1, 0

    while tweetCount < max_tweets:
        try:
            if (max_id <= 0):
                if (not sinceId):
                    new_tweets = api.search(q=qry, count=tweets_per_qry)
                else:
                    new_tweets = api.search(q=qry, count=tweets_per_qry, since_id=sinceId)
            else:
                if (not sinceId):
                    new_tweets = api.search(q=qry, count=tweets_per_qry, max_id=str(max_id - 1))
                else:
                    new_tweets = api.search(q=qry, count=tweets_per_qry, max_id=str(max_id - 1), since_id=sinceId)
            if not new_tweets:
                print('Tidak ada lagi Tweet ditemukan dengan Query="{0}"'.format(qry));
                break
            for tweet in new_tweets:
                tweet_text = f'{tweet.text}'.strip().replace('\n', '')
                if tweet_text:
                    yield prep.preprocess(tweet_text)
            max_id = new_tweets[-1].id
        except tweepy.TweepError as e:
            print("some error : " + str(e));
            break  # Aya error, keluar
