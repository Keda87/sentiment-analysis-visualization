import json

from flask import Flask, request, render_template
from flaskext.mysql import MySQL

from crawler import populate_tweets
from main import get_sentiment_result, testFromTrained, td

app = Flask(__name__)

app.config['MYSQL_DATABASE_HOST'] = 'localhost'
app.config['MYSQL_DATABASE_PORT'] = 3306
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = '12345678'
app.config['MYSQL_DATABASE_DB'] = 'sentiment'

mysql = MySQL()
mysql.init_app(app)


def _get_from_db(table_name, limit=20):
    cursor = mysql.get_db().cursor()
    cursor.execute(f'SELECT usernameTweet, text, sentiment_result FROM {table_name} LIMIT {limit}')
    data = cursor.fetchall()
    data = [
        {
            'user': i[0], 
            'tweet': i[1], 
            'sentiment': i[2]
        } 
        for i in data
    ]
    cursor.close()
    return data


def _get_total_data(table_name, is_positive=True):
    sentiment = 'POSITIF' if is_positive else 'NEGATIF'
    cursor = mysql.get_db().cursor()
    cursor.execute(f'SELECT COUNT(id) as TOTAL FROM {table_name} WHERE sentiment_result = "{sentiment}"')

    result = cursor.fetchone()[0]
    return result


def _get_debate_data(debate_period, limit=10):
    return {
        'jokowi': _get_from_db(f'd{debate_period}_j', limit),
        'prabowo': _get_from_db(f'd{debate_period}_p', limit)
    }


def _get_debate_sentiment_data(debate_period):
    return {
        'jokowi': {
            'positive': _get_total_data(f'd{debate_period}_j', True),
            'negative': _get_total_data(f'd{debate_period}_j', False),
        },
        'prabowo': {
            'positive': _get_total_data(f'd{debate_period}_p', True),
            'negative': _get_total_data(f'd{debate_period}_p', False),
        }
    }


@app.route('/')
def home():
    return render_template('index.html')


@app.route('/realtime', methods=['post', 'get'])
def realtime_page():
    if request.method == 'POST':
        keyword = request.form.get('keyword')
        data = populate_tweets(keyword)
        result = get_sentiment_result(data)
        return render_template('page1.html', data=result)

    data = {
        'POSITIF': {'total': 0, 'data': []},
        'NEGATIF': {'total': 0, 'data': []}
    }
    return render_template('page1.html', data=data)


@app.route('/display')
def display_page():
    data = {
        'debat_1': _get_debate_data(1, 20),
        'debat_2': _get_debate_data(2, 20),
        'debat_3': _get_debate_data(3, 20),
        'debat_4': _get_debate_data(4, 20),
        'debat_5': _get_debate_data(5, 20),
    }

    stats = {
        'debat_1': _get_debate_sentiment_data(1),
        'debat_2': _get_debate_sentiment_data(2),
        'debat_3': _get_debate_sentiment_data(3),
        'debat_4': _get_debate_sentiment_data(4),
        'debat_5': _get_debate_sentiment_data(5),
    }
    return render_template('page2.html', data=data, stats=json.dumps(stats))